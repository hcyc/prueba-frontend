import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormDialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'sq-form-sample',
  templateUrl: './form-sample.component.html',
  styleUrls: ['./form-sample.component.css']
})
export class FormSampleComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  buttonClick(){
    this.dialog.open(FormDialogComponent,{
      width: '250px',
    })
  }
}
